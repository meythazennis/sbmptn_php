<?php  

// kita ambil $jenis_tag dan $db yg ada di file index.php, kita masukin semua query nya didalem $soal
$soal = $db->getSoal($jenis_tag); 

// kita coba keluarin data soal
// var_dump($soal);

?>

<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">List Data TKPA SOSHUM</h3>

        <div class="box-tools">
          <div class="input-group">
            <a href="?action=tambah&id=<?php echo $jenis_tag; ?>" class="btn btn-primary">Tambah Data</a> &nbsp; 
            <button type="submit" class="btn btn-default">Generate JSON</button>
            <br>
          </div>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body table-responsive">
      <?php
        if ($id_soal && $id_soal != "") {
          if ($db->deleteSoalById($id_soal)) {
      ?>
        <script type="text/javascript">
          // window.location.reload();
        </script>
        <div class="callout callout-success">
          <p>Success Delete Data!</p>
        </div>
      <?php
          }
        }
      ?>
        <table class="table table-hover">
          <tr>
            <th>No. Soal</th>
            <th>Soal</th>
            <th>Aksi</th>
          </tr>
          <?php
            while ($item = $soal->fetch_assoc()) {
            ?>
          <tr>
            <td><?php echo $item['no_soal']; ?></td>
            <td><?php echo $item['soal']; ?></td>
            <td>
            <a href="?jenis=<?php echo $jenis_tag; ?>&id=<?php echo $jenis_tag; ?>&id_soal=<?php echo $item['id_soal'];?>" onClick="return confirm('Hapus soal ini?')"><span class="label label-danger">Delete</span></a>
             |
            <a href="?action=editdata&id=<?php echo $jenis_tag; ?>&id_soal=<?php echo $item['id_soal'];?>"><span class="label label-success">Update</span></a></td>
          </tr>
           <?php
            }
            ?>
        </table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
</div>