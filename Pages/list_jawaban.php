<?php  
$jawaban = $db->getJawabanByIdSoal($id_soal);
?>

<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">List Jawaban</h3>
      </div>

      <?php
      if ($id_jawaban && $id_jawaban != "") {
        if ($db->deleteJawabanById($id_jawaban)) {
      ?>
      <div class="callout callout-success">
        <p>Success Delete Data!</p>
      </div>
      <?php
        }
      }
      ?>

      <!-- /.box-header -->
      <div class="box-body table-responsive">
        <table class="table table-hover">
          <tr>
            <th>Jawaban</th>
            <th>Tipe</th>
            <th>Aksi</th>
          </tr>
          <?php
            while ($item = $jawaban->fetch_assoc()) {
            ?>
          <tr>
            <td><?php echo $item['jawaban']; ?></td>
            <td><?php echo $db->getTipeByIdTipe($item['id_tipe']); ?></td>
            <td>
            <a href="?action=editjawaban&id=<?php echo $id; ?>&id_soal=<?php echo $id_soal; ?>&idjawaban=<?php echo $item['id_jawaban']; ?>" onClick="return confirm('Hapus soal ini?')"><span class="label label-danger">Delete</span></a>
             |
            <a href="?action=editjawabanact&id=<?php echo $jenis_tag; ?>&id_soal=<?php echo $id_soal; ?>&id_jawaban=<?php echo $item['id_jawaban'];?>"><span class="label label-success">Update</span></a></td>
          </tr>
           <?php
            }
            ?>
        </table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
</div>