<?php
if (isset($_GET['id_jawaban'])) {
  $id_jawaban = $_GET['id_jawaban'];
  $datajawaban = $db->getJawabanById($id_jawaban);
  $datasoal = $db->getSoalById($id_soal);
  $datatipe = $db->getTipe();

  $result = 0;

  if (isset($_POST['editjawabandata'])) {
    if (strlen($_FILES['gambar']['name']) > 0) {
        $image = $db->uploadImage($_FILES['gambar'], $datasoal['no_soal'], $id, "jawaban_" . $i);
        if (strlen($image) > 0) {
          $sendArray = array(
            'jawaban' => $_POST['jawaban'],
            'gambar' => $image,
            'id_tipe' => $_POST['id_tipe'],
            'id_jawaban' => $id_jawaban
            );
          if ($db->updateJawabanById($sendArray)) {
            $result = 1;
          } else {
            $result = 2;
          }
        }
      } else {
        $sendArray = array(
            'jawaban' => $_POST['jawaban'],
            'gambar' => $_POST['hiddengambar'],
            'id_tipe' => $_POST['id_tipe'],
            'id_jawaban' => $id_jawaban
            );
          if ($db->updateJawabanById($sendArray)) {
            $result = 1;
          } else {
            $result = 2;
          }
      }
  }
?>
<script>
function goBack() {
    window.history.back();
}
</script>
<div class="row">
  <!-- left column -->
  <div class="col-md-12 ">

    <div class="box box-info">
      <div class="box-header">
        <h3 class="box-title">Edit Jawaban</h3>
      </div>
        <form action="" method="post" enctype="multipart/form-data">
          <div class="box-body">
              <?php
              if ($result == 1) {
              ?>
                <div class="callout callout-success">
                  <p>Success Update Data!</p>
                </div>
              <?php
              } elseif ($result == 2) {
              ?>
                <div class="callout callout-danger">
                  <p>Failed Update Data :(</p>
                </div>
              <?php
              }
              ?>
              <div class="form-group">
                  <label for="gambar">Masukkan Gambar</label>
                  <input type="file" name="gambar" id="gambar">
                  <img src="<?php echo $datajawaban['gambar'];  ?>" width=200px>
                  <input type="hidden" name="hiddengambar" value="<?php echo $datajawaban['gambar']; ?>"></input>
                  <p class="help-block">(.png), (.jpeg), (.jpg) Max. 50mb</p>
                </div>
              <div>
                <label for="soal">Jawaban</label>
                <textarea class="textarea" name="jawaban" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo $datajawaban['jawaban']; ?></textarea> <!-- masing2 dari mereka punya name yg di ambil di fungsi diatas -->
              </div>
              <div class="form-group">
                <label>Pilih Tipe</label>
                <select class="form-control" name="id_tipe">
                <?php
                while ($item = $datatipe->fetch_assoc()) {
                ?>
                  <option value="<?php echo $item['id_tipe']; ?>"  <?php echo ($item['id_tipe'] == $datajawaban['id_tipe']) ? "selected" : ""; ?>> <?php echo $item['tipe']; ?> </option>
                <?php
                }
                ?>
                  </select>
              </div>
          </div>
          <div class="box-footer clearfix">
            <button onClick="goBack()" class="btn btn-success">Back</button>
            <input type="submit" name="editjawabandata" value="Submit" class="pull-right btn btn-primary"/>  <!-- begitu juga ini diambil name nya -->
            <input type="reset" value="Reset" class="pull-right btn btn-success"/> <!-- kalau ini gak perlu deh :v begini jg udh berfungsi -->
          </div>
        </form>
    </div>
  </div>  
</div>
<?php
}
?>