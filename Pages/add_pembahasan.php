<?php

$soal = $db->getSoalById($id_soal); // hahaha aku lupa
$jawaban = $db->getJawabanByIdSoal($id_soal);

$result = 0;

if (isset($_POST['tambahdata'])) {
  $id_jawaban = $_POST['id_jawaban'];
  if (strlen($_FILES['gambar']['name']) > 0) { 
      $image = $db->uploadImage($_FILES['gambar'], $id_soal, $id, "pembahasan");
      if (strlen($image) > 0) {
        $sendArray = array(
          'pembahasan' => $_POST['pembahasan'],
          'gambar' => $image,
          'id_jawaban' => $id_jawaban
          );
        $id_pembahasan = $db->tambahPembahasan($sendArray);
        if (strlen($id_pembahasan) > 0) {
          if ($db->updateIdJenisIdPembahasanSoalByIdSoal($id, $id_pembahasan, $id_jawaban, $id_soal)) {
            $result = 1;
          } else {
            $result = 2;
          }
        } else {
          $result = 2;
      }
     } 
    } else {
      $sendArray = array(
          'pembahasan' => $_POST['pembahasan'],
          'gambar' => "",
          'id_jawaban' => $id_jawaban
          );
        $id_pembahasan = $db->tambahPembahasan($sendArray);
        if (strlen($id_pembahasan) > 0) {
          if ($db->updateIdJenisIdPembahasanSoalByIdSoal($id, $id_pembahasan, $id_jawaban, $id_soal)) {
            $result = 1;
          } else {
            $result = 2;
          }
        } else {
          $result = 2;
        }
    }
}
?>
<div class="row">
  <!-- left column -->
  <div class="col-md-12 ">

    <div class="box box-info">
      <div class="box-header">
        <h3 class="box-title">Tambah Pembahasan</h3>
      </div>
        <div class="box-body">
              <?php
              if ($result == 1) {
              ?>
                <div class="callout callout-success">
                  <h4>Success Insert Data!</h4>

                  <p>Back to list?</p>
                  <a href="?jenis=<?php echo $id;?>" class=" btn btn-primary">List</a>
                </div>
              <?php
              } elseif ($result == 2) {
              ?>
                <div class="callout callout-danger">
                  <h4>Failed Insert Data :(</h4>

                  <p>So Sorry..</p>
                </div>
              <?php
              }
              ?>
          
            <div class="form-group">
                <label for="no soal">No. Soal</label>
                <p><?php echo $soal['no_soal'] ; ?></p>
            </div>                   

            <div class="form-group">
              <label for="no soal">Soal</label>
              <?php echo $soal['soal'] ; ?>
            </div>
        <form action="" method="post" enctype="multipart/form-data">
              <div class="form-group">
                  <label for="gambar">Masukkan Gambar</label>
                  <input type="file" name="gambar" id="gambar">
                  <p class="help-block">(.png), (.jpeg), (.jpg) Max. 50mb</p>
                </div>
              <div>
                <label for="pembahasan">Pembahasan</label>
                <textarea class="textarea" name="pembahasan" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
              </div>
              <div class="form-group">
                <label>Pilih Jawaban yg Benar</label>
                <select class="form-control" name="id_jawaban">
                <?php
                while ($item = $jawaban->fetch_assoc()) {
                  echo "<option value=\"" . $item['id_jawaban'] . "\">" . $item['jawaban'] . "</option>";
                }
                ?>
                  </select>
              </div>
          </div>
          <div class="box-footer clearfix">
            <input type="submit" name="tambahdata" value="Submit" class="pull-right btn btn-primary"/>  
            <input type="reset" value="Reset" class="pull-right btn btn-success"/>
          </div>
        </form>
    </div>
  </div>  
</div>