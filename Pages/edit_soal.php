<?php

$datasoal = $db->getAllFieldSoalById($id_soal);
$id_pembahasan = $db->getIdPembahasanByIdSoal($id_soal);

$result = 0;

if (isset($_POST['edit'])) {
  if (strlen($_FILES['gambar']['name']) > 0) {
      $image = $db->uploadImage($_FILES['gambar'], $_POST['no_soal'], $id, "tkd");
      if (strlen($image) > 0) {
        $sendArray = array(
          'no_soal' => $_POST['no_soal'],
          'gambar' => $image,
          'soal' => $_POST['soal'],
          'id_soal' => $id_soal
          );
        if ($db->updateSoalById($sendArray)) {
          $result = 1;
        } else {
          $result = 2;
        }
      }
    } else {
      $sendArray = array(
          'no_soal' => $_POST['no_soal'],
          'gambar' => $_POST['hiddengambar'],
          'soal' => $_POST['soal'],
          'id_soal' => $id_soal
          );
        if ($db->updateSoalById($sendArray)) {
          $result = 1;
        } else {
          $result = 2;
        }
    }
}
?>
<div class="row">
  <!-- left column -->
  <div class="col-md-12 ">

    <div class="box box-info">
      <div class="box-header">
        <h3 class="box-title">Edit Soal</h3>
      </div>
        <form action="" method="post" enctype="multipart/form-data">
          <div class="box-body">
              <?php
              if ($result == 1) {
              ?>
                <div class="callout callout-success">
                  <p>Success Update Data!</p>
                </div>
              <?php
              } elseif ($result == 2) {
              ?>
                <div class="callout callout-danger">
                  <p>Failed Update Data :(</p>
                </div>
              <?php
              }
              ?>
              <div class="callout callout-warning">
                <a href="?action=editjawaban&id=<?php echo $id; ?>&id_soal=<?php echo $id_soal; ?>"><p>Edit Jawaban</p></a>
                <a href="?action=editpembahasan&id_pembahasan=<?php echo $id_pembahasan; ?>&id=<?php echo $id; ?>&id_soal=<?php echo $id_soal; ?>"><p>Edit Pembahasan</p></a>
              </div>
              <div class="form-group">
                <label for="no soal">No. Soal</label>
                <input type="text" class="form-control" name="no_soal" value="<?php echo $datasoal['no_soal']; ?>">
              </div>
              <div class="form-group">
                  <label for="gambar">Masukkan Gambar</label>
                  <input type="file" name="gambar" id="gambar">
                  <img src="<?php echo $datasoal['gambar'];  ?>" width=200px>
                  <input type="hidden" name="hiddengambar" value="<?php echo $datasoal['gambar']; ?>"></input>
                  <p class="help-block">(.png), (.jpeg), (.jpg) Max. 50mb</p>
                </div>
              <div>
                <label for="soal">Soal</label>
                <textarea class="textarea" name="soal" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo $datasoal['soal']; ?></textarea> <!-- masing2 dari mereka punya name yg di ambil di fungsi diatas -->
              </div>
          </div>
          <div class="box-footer clearfix">
            <input type="submit" name="edit" value="Submit" class="pull-right btn btn-primary"/>  <!-- begitu juga ini diambil name nya -->
            <input type="reset" value="Reset" class="pull-right btn btn-success"/> <!-- kalau ini gak perlu deh :v begini jg udh berfungsi -->
          </div>
        </form>
    </div>
  </div>  
</div>