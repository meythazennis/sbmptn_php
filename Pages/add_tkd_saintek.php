<?php

$id_soal = "";
$result = 0;

if (isset($_POST['tambahdata'])) { // aku ambil pake $_POST karna method="post" liat lagi dibawah hwhw
  if (strlen($_FILES['gambar']['name']) > 0) { // aku cek dulu user nya nge upload file atau ngga, dengan parameter nama file nya, kalau nama file nya ada atau karakter nya > 0 maka lakuin fungsi dibawahnya
      $image = $db->uploadImage($_FILES['gambar'], $_POST['no_soal'], $id, "tkd"); // tag "gambar" aku ambil dari name dibawah, tag no_soal aku ambil dari no_soal yg diinput dibawah, $jenis_tag dari index.php, "tkd" aku yg namain sendiri karna ini buat tkd
      if (strlen($image) > 0) {
        $sendArray = array(
          'no_soal' => $_POST['no_soal'],
          'gambar' => $image,
          'soal' => $_POST['soal']
          );
        $id_soal = $db->tambahSoal($sendArray, $id); // kalau ini berhasil dia ngeluarin id_soal
        if (strlen($id_soal) > 0) {
          $result = 1;
        } else {
          $result = 2;
        }
      }
    } else {
      $sendArray = array(
          'no_soal' => $_POST['no_soal'],
          'gambar' => "",
          'soal' => $_POST['soal']
          );
        $id_soal = $db->tambahSoal($sendArray, $id); // kalau ini berhasil dia ngeluarin id_soal
        if (strlen($id_soal) > 0) {
          $result = 1;
        } else {
          $result = 2;
        }
    }
}
?>
<div class="row">
  <!-- left column -->
  <div class="col-md-12 ">

    <div class="box box-info">
      <div class="box-header">
        <h3 class="box-title">Tambah Soal TKD Saintek</h3>
      </div>
        <form action="" method="post" enctype="multipart/form-data"> <!-- action="" ini dikosongin supaya aksinya tetep ke dirinya sendiri, dan fungsinya aku buat diatas tuh ^ -->
          <div class="box-body">
              <?php
              if ($result == 1) {
              ?>
                <div class="callout callout-success">
                  <h4>Success Insert Data!</h4>

                  <p>Try to Insert Jawaban </p>
                  <a href="?action=tambahjawaban&id=<?php echo $id;?>&id_soal=<?php echo $id_soal;?>" class=" btn btn-primary">Create</a>
                </div>
              <?php
              } elseif ($result == 2) {
              ?>
                <div class="callout callout-danger">
                  <h4>Failed Insert Data :(</h4>

                  <p>So Sorry..</p>
                </div>
              <?php
              }
              ?>
              <div class="form-group">
                <label for="no soal">No. Soal</label>
                <input type="text" class="form-control" name="no_soal"> <!-- masing2 dari mereka punya name yg di ambil di fungsi diatas -->
              </div>
              <div class="form-group">
                  <label for="gambar">Masukkan Gambar</label>
                  <input type="file" name="gambar" id="gambar"> <!-- masing2 dari mereka punya name yg di ambil di fungsi diatas -->
                  <p class="help-block">(.png), (.jpeg), (.jpg) Max. 50mb</p>
                </div>
              <div>
                <label for="soal">Soal</label>
                <textarea class="textarea" name="soal" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea> <!-- masing2 dari mereka punya name yg di ambil di fungsi diatas -->
              </div>
          </div>
          <div class="box-footer clearfix">
            <input type="submit" name="tambahdata" value="Submit" class="pull-right btn btn-primary"/>  <!-- begitu juga ini diambil name nya -->
            <input type="reset" value="Reset" class="pull-right btn btn-success"/> <!-- kalau ini gak perlu deh :v begini jg udh berfungsi -->
          </div>
        </form>
    </div>
  </div>  
</div>