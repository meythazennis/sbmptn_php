<?php
$soal = $db->getSoalById($id_soal);
$tipe = $db->getTipe();

$id_jawaban = "";
$result = 0;

 // ambil last elemen dari array, buat nge cek kalau ini terakhir looping

if (isset($_POST['tambahdata'])) {
  for ($i=1; $i <= $tipe->num_rows; $i++) { 
    $post_gambar =  "gambar" . $i;
    $post_jawaban = "jawaban" . $i;
    if (strlen($_FILES[$post_gambar]['name']) > 0) {
      $image = $db->uploadImage($_FILES[$post_gambar], $soal['no_soal'], $id, "jawaban_" . $i); 
        $sendArray = array(
          'jawaban' => $_POST[$post_jawaban],
          'gambar' => $image,
          'id_soal' => $id_soal,
          'id_tipe' => $i,
          );
        $id_jawaban = $db->tambahJawaban($sendArray);
        if ($i == $tipe->num_rows) {
          if (strlen($id_jawaban) > 0) {
            $result = 1;
          } else {
            $result = 2;
          }
        }
    } else {
      $sendArray = array(
          'jawaban' => $_POST[$post_jawaban],
          'gambar' => "",
          'id_soal' => $id_soal,
          'id_tipe' => $i,
          );
      $id_jawaban = $db->tambahJawaban($sendArray);
      if ($i == $tipe->num_rows) {
        if (strlen($id_jawaban) > 0) {
          $result = 1;
         } else {
          $result = 2;
        }
      }
    }
  }
}

?>
<div class="row">
  <!-- left column -->
  <div class="col-md-12 ">

    <div class="box box-info">
      <div class="box-header">
        <h3 class="box-title">Tambah Jawaban</h3>
      </div>
          <div class="box-body">
            <?php
              if ($result == 1) {
              ?>
                <div class="callout callout-success">
                  <h4>Success Insert Data!</h4>

                  <p>Try to Insert Pembahasan </p>
                  <a href="?action=tambahpembahasan&id=<?php echo $id;?>&id_soal=<?php echo $id_soal;?>" class=" btn btn-primary">Create</a>
                </div>
              <?php
              } elseif ($result == 2) {
              ?>
                <div class="callout callout-danger">
                  <h4>Failed Insert Data :(</h4>

                  <p>So Sorry..</p>
                </div>
              <?php
              }
              ?>
            <div class="form-group">
              <label for="no soal">No. Soal</label>
              <p><?php echo $soal['no_soal'] ; ?></p>
            </div>
            <div class="form-group">
              <label for="no soal">Soal</label>
              <?php echo $soal['soal'] ; ?>
            </div>
        <form action="" method="post" enctype="multipart/form-data">
            <?php
            while ($item = $tipe->fetch_assoc()) {
            ?>
            <input type="hidden" name=""></input>
            <div class="form-group">
                <label>Tipe</label>
                <p><?php echo $item['tipe']; ?></p>
              </div>
            <div>
            <div class="form-group">
                <label>Masukkan Gambar</label>
                <input type="file" name="gambar<?php echo $item['id_tipe']; ?>" id="gambar"> 
                <p class="help-block">(.png), (.jpeg), (.jpg) Max. 50mb</p>
              </div>
            <div>
              <label>Jawaban</label>
              <textarea class="textarea" name="jawaban<?php echo $item['id_tipe']; ?>" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea> 
            </div>
            <br>
            <?php
            }
            ?>  
          </div>
          <div class="box-footer clearfix">
            <input type="submit" name="tambahdata" value="Submit" class="pull-right btn btn-primary"/> 
            <input type="reset" value="Reset" class="pull-right btn btn-success"/> 
          </div>
        </form>
    </div>
  </div>  
</div>