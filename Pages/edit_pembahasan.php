<?php

$id_pembahasan = $db->getIdPembahasanByIdSoal($id_soal);
$datapembahasan = $db->getPembahasanByIdPembahasan($id_pembahasan);
$datajawaban = $db->getJawabanByIdSoal($id_soal);

$result = 0;

if (isset($_POST['edit'])) {
  if (strlen($_FILES['gambar']['name']) > 0) {
      $image = $db->uploadImage($_FILES['gambar'], $id_soal, $id, "pembahasan");
      if (strlen($image) > 0) {
        $sendArray = array(
          'pembahasan' => $_POST['pembahasan'],
          'gambar' => $image,
          'id_jawaban' => $_POST['id_jawaban'],
          'id_pembahasan' => $id_pembahasan
          );
        if ($db->updatePembahasanById($sendArray)) {
          $result = 1;
        } else {
          $result = 2;
        }
      }
    } else {
      $sendArray = array(
          'pembahasan' => $_POST['pembahasan'],
          'gambar' => $_POST['hiddengambar'],
          'id_jawaban' => $_POST['id_jawaban'],
          'id_pembahasan' => $id_pembahasan
          );
        if ($db->updatePembahasanById($sendArray)) {
          $result = 1;
        } else {
          $result = 2;
        }
    }
}
?>
<div class="row">
  <!-- left column -->
  <div class="col-md-12 ">

    <div class="box box-info">
      <div class="box-header">
        <h3 class="box-title">Edit Pembahasan</h3>
      </div>
        <form action="" method="post" enctype="multipart/form-data">
          <div class="box-body">
              <?php
              if ($result == 1) {
              ?>
                <div class="callout callout-success">
                  <p>Success Update Data!</p>
                </div>
              <?php
              } elseif ($result == 2) {
              ?>
                <div class="callout callout-danger">
                  <p>Failed Update Data :(</p>
                </div>
              <?php
              }
              ?>
              <div class="form-group">
                  <label for="gambar">Masukkan Gambar</label>
                  <input type="file" name="gambar" id="gambar">
                  <img src="<?php echo $datapembahasan['gambar'];  ?>" width=200px>
                  <input type="hidden" name="hiddengambar" value="<?php echo $datapembahasan['gambar']; ?>"></input>
                  <p class="help-block">(.png), (.jpeg), (.jpg) Max. 50mb</p>
                </div>
              <div>
                <label for="soal">Pembahasan</label>
                <textarea class="textarea" name="pembahasan" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo $datapembahasan['pembahasan']; ?></textarea> <!-- masing2 dari mereka punya name yg di ambil di fungsi diatas -->
              </div>
              <div class="form-group">
                <label>Pilih Jawaban</label>
                <select class="form-control" name="id_jawaban">
                <?php
                while ($item = $datajawaban->fetch_assoc()) {
                ?>
                  <option value="<?php echo $item['id_jawaban']; ?>"  <?php echo ($item['id_jawaban'] == $datapembahasan['id_jawaban']) ? "selected" : ""; ?>> <?php echo $item['jawaban']; ?> </option>
                <?php
                }
                ?>
                  </select>
              </div>
          </div>
          <div class="box-footer clearfix">
            <input type="submit" name="edit" value="Submit" class="pull-right btn btn-primary"/>  <!-- begitu juga ini diambil name nya -->
            <input type="reset" value="Reset" class="pull-right btn btn-success"/> <!-- kalau ini gak perlu deh :v begini jg udh berfungsi -->
          </div>
        </form>
    </div>
  </div>  
</div>