<?php

class Base
{
	private $conn;

    function __construct() {
        require_once 'DbConnect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }

	public function getJurusan()
	{
		$stmt = $this->conn->prepare("SELECT * FROM jurusan");
        $stmt->execute();
        $jurusan = $stmt->get_result();
        $stmt->close();
        return $jurusan;
	}

	public function getJenisByIdJurusan($id_jurusan)
	{
		$stmt = $this->conn->prepare("SELECT * FROM jenis WHERE id_jurusan = ?");
        $stmt->bind_param("i", $id_jurusan);
        $stmt->execute();
        $jenis = $stmt->get_result();
        $stmt->close();
        return $jenis;
	}

    public function getSoal($id_jenis)
    {
        $stmt = $this->conn->prepare("SELECT * FROM soal WHERE id_jenis = ?");
        $stmt->bind_param("i", $id_jenis);
        $stmt->execute();
        $soal = $stmt->get_result();
        $stmt->close();
        return $soal;
    }

    public function getJawabanByIdSoal($id_soal)
    {
        $stmt = $this->conn->prepare("SELECT * FROM jawaban WHERE id_soal = ?");
        $stmt->bind_param("i", $id_soal);
        $stmt->execute();
        $jawaban = $stmt->get_result();
        $stmt->close();
        return $jawaban;
    }

    public function getSoalById($id)
    {
        $stmt = $this->conn->prepare("SELECT no_soal, soal FROM soal WHERE id_soal = ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        if ($stmt->execute()) {
            $stmt->bind_result($no_soal, $data_soal);
            $stmt->fetch();
            $soal = array();
            $soal["no_soal"] = $no_soal;
            $soal["soal"] = $data_soal;
            return $soal;
        } 
        return $soal;
    }

    public function getTipeById($id)
    {
        $stmt = $this->conn->prepare("SELECT * FROM tipe WHERE id_tipe = ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $tipe = $stmt->get_result();
        $stmt->close();
        return $tipe;
    }

    public function getAllFieldSoalById($id)
    {
        $stmt = $this->conn->prepare("SELECT no_soal, soal as datasoal, gambar, id_pembahasan FROM soal WHERE id_soal = ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        if ($stmt->execute()) {
            $stmt->bind_result($no_soal, $datasoal, $gambar, $id_pembahasan);
            $stmt->fetch();
            $soal = array();
            $soal["no_soal"] = $no_soal;
            $soal["soal"] = $datasoal;
            $soal["gambar"] = $gambar;
            $soal["id_pembahasan"] = $id_pembahasan;
            return $soal;
        } 
        return $soal;
    }

    public function getJawabanById($id)
    {
        $stmt = $this->conn->prepare("SELECT jawaban as datajawaban, gambar, id_tipe FROM jawaban WHERE id_jawaban = ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        if ($stmt->execute()) {
            $stmt->bind_result($datajawaban, $gambar, $id_tipe);
            $stmt->fetch();
            $jawaban = array();
            $jawaban["jawaban"] = $datajawaban;
            $jawaban["gambar"] = $gambar;
            $jawaban["id_tipe"] = $id_tipe;
            return $jawaban;
        } 
        return $jawaban;
    }

    public function getPembahasanByIdPembahasan($id)
    {
        $stmt = $this->conn->prepare("SELECT pembahasan as datapem, gambar, id_jawaban FROM pembahasan WHERE id_pembahasan = ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        if ($stmt->execute()) {
            $stmt->bind_result($datapem, $gambar, $id_jawaban);
            $stmt->fetch();
            $pembahasan = array();
            $pembahasan["pembahasan"] = $datapem;
            $pembahasan["gambar"] = $gambar;
            $pembahasan["id_jawaban"] = $id_jawaban;
            return $pembahasan;
        } 
        return $pembahasan;
    }

    public function getIdPembahasanByIdSoal($id_soal)
    {
        $stmt = $this->conn->prepare("SELECT id_pembahasan FROM soal WHERE id_soal = ?");
        $stmt->bind_param("i", $id_soal);
        $stmt->execute();
        if ($stmt->execute()) {
            $stmt->bind_result($id_pembahasan);
            $stmt->fetch();
            return $id_pembahasan;
        } 
        return $id_pembahasan;
    }

    public function getTipeByIdTipe($id)
    {
        $stmt = $this->conn->prepare("SELECT tipe FROM tipe WHERE id_tipe = ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        if ($stmt->execute()) {
            $stmt->bind_result($tipe);
            $stmt->fetch();
            return $tipe;
        } 
        return $tipe;
    }

    public function tambahSoal($array, $id_jenis) // $array >> aku masukkin semua data soal ke dalem $array
    {
        $stmt = $this->conn->prepare("INSERT INTO soal(no_soal, soal, gambar, id_jenis) VALUES(?, ?, ?, ?)");
        $stmt->bind_param("issi", $array['no_soal'], $array['soal'], $array['gambar'], $id_jenis);
        $result = $stmt->execute();
        $stmt->close();
        if ($result) {
            return $this->conn->insert_id;
        } else {
            return NULL;
        }
    }

    public function tambahJawaban($array)
    {
        $stmt = $this->conn->prepare("INSERT INTO jawaban(jawaban, gambar, id_soal, id_tipe) VALUES(?, ?, ?, ?)");
        $stmt->bind_param("sssi", $array['jawaban'], $array['gambar'], $array['id_soal'], $array['id_tipe']);
        $result = $stmt->execute();
        $stmt->close();
        if ($result) {
            return $this->conn->insert_id;
        } else {
            return NULL;
        }
    }

    public function getTipe()
    {
        $stmt = $this->conn->prepare("SELECT * FROM tipe");
        $stmt->execute();
        $tipe = $stmt->get_result();
        $stmt->close();
        return $tipe;
    }

    public function tambahPembahasan($array)
    {
        $stmt = $this->conn->prepare("INSERT INTO pembahasan(pembahasan, gambar, id_jawaban) VALUES(?, ?, ?)");
        $stmt->bind_param("ssi", $array['pembahasan'], $array['gambar'], $array['id_jawaban']);
        $result = $stmt->execute();
        $stmt->close();
        if ($result) {
            return $this->conn->insert_id;
        } else {
            return NULL;
        }
    }

    public function updateIdJenisIdPembahasanSoalByIdSoal($id_jenis, $id_pembahasan, $id_jawaban, $id_soal) {
        $stmt = $this->conn->prepare("UPDATE soal set id_jenis = ?, id_pembahasan = ?, id_jawaban = ? WHERE id_soal = ?");
        $stmt->bind_param("iiii", $id_jenis, $id_pembahasan, $id_jawaban, $id_soal);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();
        return $num_affected_rows > 0;
    }

    public function updateSoalById($array)
    {
        $stmt = $this->conn->prepare("UPDATE soal set no_soal = ?, soal = ?, gambar = ? WHERE id_soal = ?");
        $stmt->bind_param("sssi", $array['no_soal'], $array['soal'], $array['gambar'], $array['id_soal']);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();
        return $num_affected_rows > 0;
    }

    public function updateJawabanById($array)
    {
        $stmt = $this->conn->prepare("UPDATE jawaban set jawaban = ?, id_tipe = ?, gambar = ? WHERE id_jawaban = ?");
        $stmt->bind_param("sisi", $array['jawaban'], $array['id_tipe'], $array['gambar'], $array['id_jawaban']);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();
        return $num_affected_rows > 0;
    }

    public function updatePembahasanById($array)
    {
        $stmt = $this->conn->prepare("UPDATE pembahasan set pembahasan = ?, id_jawaban = ?, gambar = ? WHERE id_pembahasan = ?");
        $stmt->bind_param("sisi", $array['pembahasan'], $array['id_jawaban'], $array['gambar'], $array['id_pembahasan']);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();
        return $num_affected_rows > 0;
    }

    public function deleteSoalById($id_soal)
    {
        if ($this->deleteJawaban($id_soal)) { // delete jawaban nya dulu
           $id_pembahasan = $this->getIdPembahasanByIdSoal($id_soal); // get id pembahasannya
           if ($this->deletePembahasan($id_pembahasan)) { // delete pembahasannya
                $stmt = $this->conn->prepare("DELETE FROM soal WHERE id_soal = ?"); // baru deh delete soalnya
                $stmt->bind_param("i", $id_soal);
                $stmt->execute();
                $num_affected_rows = $stmt->affected_rows;
                $stmt->close();
                return $num_affected_rows > 0; // az
           }
        }
        return false;
    }

    public function deleteJawaban($id_soal)
    {
        $stmt = $this->conn->prepare("DELETE FROM jawaban WHERE id_soal = ?");
        $stmt->bind_param("i", $id_soal);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();
        return $num_affected_rows > 0;
    }

    public function deleteJawabanById($id)
    {
        $stmt = $this->conn->prepare("DELETE FROM jawaban WHERE id_jawaban = ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();
        return $num_affected_rows > 0;
    }

    public function deletePembahasan($id_pembahasan)
    {
        $stmt = $this->conn->prepare("DELETE FROM pembahasan WHERE id_pembahasan = ?");
        $stmt->bind_param("i", $id_pembahasan);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();
        return $num_affected_rows > 0;
    }

    public function uploadImage($file, $no_soal, $id_jenis, $type)
    {
        $target_dir = "src/" . $id_jenis . "/";
        $target_file = $target_dir . basename($file["name"]);
        $file_name = $target_dir . $type . "_" . $no_soal . ".jpg";
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        // Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($file["tmp_name"]);
            if($check !== false) {
                echo "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                echo "File is not an image.";
                $uploadOk = 0;
            }
        }
        // Check if file already exists
        // if (file_exists($target_file)) {
        //     echo "Sorry, file already exists.";
        //     $uploadOk = 0;
        // }
        // Check file size
        if ($file["size"] > 500000) {
            echo "Sorry, your file is too large.";
            $uploadOk = 0;
        }
        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
            echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($file["tmp_name"], $file_name)) {
                return $file_name;
            } else {
                return;
            }
        }
        return;
    }
}
?>