<?php 
require_once 'base.php';
$base_url = "http://localhost:8080/sbmptn_php";
$path_assets = $base_url . "/assets/"; 

$db = new Base();
$jurusan = $db->getJurusan();

$jenis_tag = "";

if (isset($_GET['jenis'])) {
  $jenis_tag = $_GET['jenis'];
}

$id = "";
$action_tag = "";
$id_soal = "";
$id_jawaban = "";

if (isset($_GET['action']) && isset($_GET['id'])) {
  $action_tag = $_GET['action'];
  $id = $_GET['id'];
  //$id_soal = $_GET['id_soal']; // yg undefined index yg ini kan? itu karena $_GET['id_soal'] << ini gak ada, dan gak kamu prevent(cegah) ketika gak ada, mangkanya dia bilang undefined
}

if (isset($_GET['id_soal'])) { // disini guna nya isset, iset itu kalau dia ada baru dilakuin
  $id_soal = $_GET['id_soal']; // kalau udh kayak gini, gak bakal muncul error undefined lagi. jadi undefined itu muncul pas variable gak ada isinya aja
}

if (isset($_GET['idjawaban'])) {
  $id_jawaban = $_GET['idjawaban'];
}

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo $path_assets; ?>bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo $path_assets; ?>dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo $path_assets; ?>dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo $path_assets; ?>plugins/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?php echo $path_assets; ?>plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo $path_assets; ?>plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo $path_assets; ?>plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo $path_assets; ?>plugins/daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo $path_assets; ?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <!-- https1ML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo $base_url; ?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Admin</b>LTE</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo $path_assets; ?>dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Alexander Pierce</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <?php
        while ($item = $jurusan->fetch_assoc()) {
		    ?>
			   <li class="treeview">
	          <a href="#">
	            <span><?php echo $item['jurusan']; ?></span>
	            <span class="pull-right-container">
	              <i class="fa fa-angle-left pull-right"></i>
	            </span>
	          </a>
	          <ul class="treeview-menu">
	         	 <?php 
		          $jenis = $db->getJenisByIdJurusan($item['id_jurusan']);
		          while ($item_jenis = $jenis->fetch_assoc()) {
		          ?>
		          	<li><a href="?jenis=<?php echo $item_jenis['id_jenis']; ?>"><?php echo $item_jenis['jenis']; ?></a></li>
		          <?php
		          }
		          ?>
	          </ul>
	        </li>
    		<?php
    		}
        ?>

    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo $base_url; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      
      <!-- Main row -->
      <?php

      if ($jenis_tag && $jenis_tag != "") { // kalau ini buat include list2 data
      	switch ($jenis_tag) {
      		case 1:
      			include 'pages/list_tkpa_saintek.php';
      		break;
      		case 2:
      			include 'pages/list_tkd_saintek.php';
      		break;
      		case 3:
      			include 'pages/list_tkpa_soshum.php';
      		break;
      		case 4:
      			include 'pages/list_tkd_soshum.php';
      		break;
      	}
      }

      if ($action_tag && $action_tag != "") { // kalau ini buat include tambah data
        if ($action_tag == "tambah") {
          switch ($id) {
            case 1:
              include 'pages/add_tkpa_saintek.php';
            break;
            case 2:
              include 'pages/add_tkd_saintek.php';
            break;
            case 3:
              include 'pages/add_tkpa_soshum.php';
            break;
            case 4:
              include 'pages/add_tkd_soshum.php';
            break;
          }
        } elseif ($action_tag == "tambahjawaban") {
          include 'pages/add_jawaban.php';
        } elseif ($action_tag == "tambahpembahasan") {
          include 'pages/add_pembahasan.php';
        } elseif ($action_tag == "editdata") {
          include 'pages/edit_soal.php';
        } elseif ($action_tag == "editjawaban") {
          include 'pages/list_jawaban.php';
        } elseif ($action_tag == "editjawabanact") {
          include 'pages/edit_jawaban.php';
        } elseif ($action_tag == "editpembahasan") {
          include 'pages/edit_pembahasan.php';
        }
      }
      ?>
        
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.6
    </div>
    <strong>Copyright &copy; <?php echo date('Y'); ?></strong> All rights
    reserved.
  </footer>

  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="<?php echo $path_assets; ?>plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo $path_assets; ?>bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?php echo $path_assets; ?>plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo $path_assets; ?>plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo $path_assets; ?>plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo $path_assets; ?>plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo $path_assets; ?>plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="<?php echo $path_assets; ?>https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?php echo $path_assets; ?>plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo $path_assets; ?>plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo $path_assets; ?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo $path_assets; ?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo $path_assets; ?>plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo $path_assets; ?>dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo $path_assets; ?>dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo $path_assets; ?>dist/js/demo.js"></script>
</body>
</html>